import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Country} from '../interfaces/country.interface';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CapitalService {

  private apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient) { }

  searchCapital(term: string): Observable<Country[]>{
    const url = `${this.apiUrl}/capital/${term}`;
    return this.http.get<Country[]>(url);
  }
}
