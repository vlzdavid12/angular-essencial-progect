import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {City} from '../interfaces/city.interface';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  private apiUrl: string = environment.apiURL;
  term = '';

  constructor(private http: HttpClient) { }

  searchCity(term: string){
   const params = new HttpParams()
    .set('fields', 'name,capital,currencies,flag,population,alpha2Code');
   this.term = term;
   const url = `${this.apiUrl}/regionalbloc/${this.term}`;
   return this.http.get<City[]>(url, {params});
  }

}
