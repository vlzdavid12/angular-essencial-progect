import {NgModule} from '@angular/core';
// Modules
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
// Components
import {ForCapitalComponent} from './pages/for-capital/for-capital.component';
import {ForCityComponent} from './pages/for-city/for-city.component';
import {ForCountryComponent} from './pages/for-country/for-country.component';
import { ShowCountryComponent } from './pages/show-country/show-country.component';
import { CityTableComponent } from './components/city-table/city-table.component';
import { CityInputComponent } from './components/city-input/city-input.component';



@NgModule({
  declarations: [
    ForCapitalComponent,
    ForCityComponent,
    ForCountryComponent,
    ShowCountryComponent,
    CityTableComponent,
    CityInputComponent,
  ],
  exports:[
    ForCapitalComponent,
    ForCityComponent,
    ForCountryComponent,
    ShowCountryComponent,
    CityTableComponent,
    CityInputComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule
  ]
})
export class CityModule {
}
