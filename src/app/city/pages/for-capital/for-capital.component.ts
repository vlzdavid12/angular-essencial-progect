import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Capital} from '../../interfaces/capital.interface';
import {CapitalService} from '../../services/capital.service';

@Component({
  selector: 'app-for-capital',
  templateUrl: './for-capital.component.html',
  styles: []
})
export class ForCapitalComponent implements OnInit {

  term: string = '';
  resultCapital: Capital[] = [];
  notFound: boolean = false;


  constructor(private capitalService: CapitalService) {
  }

  ngOnInit(): void {
  }

  searchCapital(term: string) {
    this.term = term;
    const data: Observable<Capital[]> = this.capitalService.searchCapital(this.term);
    data.subscribe(
      (resp: any) => {
        if (resp.status) {
          this.notFound = true;
          this.resultCapital = [];
          return;
        }

        this.notFound = false;
        this.resultCapital = resp;
      },
      err => console.log(err),
      () => console.log('Success fully consult for search country')
    );

  }


}
