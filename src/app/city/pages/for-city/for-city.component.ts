import { Component, OnInit } from '@angular/core';
import {CityService} from '../../services/city.service';
import {City} from '../../interfaces/city.interface';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-for-city',
  templateUrl: './for-city.component.html',
  styles: [`
  .list-btn  button{
      margin: 2px;
  }
  `
  ]
})
export class ForCityComponent implements OnInit {

  regions: string[] = ['EU',  'EFTA', 'CARICOM', 'PA', 'AU', 'EEU' , 'AL', 'CAIS' , 'CEFTA', 'NAFTA', 'SAARC'];

  regionActive: string = '';

  dataRegion: City[] = [];

  constructor(private cityService: CityService) { }

  ngOnInit(): void {
    this.activeRegion('EU')
  }

  activeRegion(region?: string){
    if ( region === this.regionActive ) {return};
    const data: Observable<City[]> = this.cityService.searchCity(region);
    data.subscribe((item: City[]) => {
      this.dataRegion = item;
      this.regionActive = region;
      },
      (error) => console.log('Error: ' + error),
      () => console.log('Success fully filter region')
    );
  }

  getClassRegion(region: string){
    return (region === this.regionActive) ? 'btn btn-primary  active' : 'btn btn-outline-primary';
  }


}
