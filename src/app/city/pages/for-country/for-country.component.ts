import {Component, OnInit} from '@angular/core';
import {CountryService} from '../../services/country.service';
import {Observable} from 'rxjs';
import {Country} from '../../interfaces/country.interface';

@Component({
  selector: 'app-for-country',
  templateUrl: './for-country.component.html',
  styles: [`
  li{
    cursor: pointer;
  }`]
})
export class ForCountryComponent implements OnInit {

  term: string = '';
  notFound: boolean = false;

  resultCountry: Country[] = [];
  subjCountry: Country[] = [];


  constructor(private countryService: CountryService) {
  }

  ngOnInit(): void {
  }

  searchCountry(term: string) {
    this.term = term;
    const data: Observable<Country[]> = this.countryService.searchCountry(this.term);
    data.subscribe(
      (resp: any) => {
        if (resp.status) {
          this.notFound = true;
          this.resultCountry = [];
          return;
        }

        this.notFound = false;
        console.log(resp);
        this.resultCountry = resp;
      },
      err => console.log(err),
      () => console.log('Success fully consult for search country')
    );

  }

  subjectCountry(term: string){
    this.notFound = false;
    this.countryService.searchCountry(term).subscribe(item => this.subjCountry = item.splice(0, 6));

  }


}
