import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CountryService} from '../../services/country.service';
import {Country} from '../../interfaces/country.interface';
import {switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-show-country',
  templateUrl: './show-country.component.html',
  styleUrls: ['./show-country.component.scss']
})
export class ShowCountryComponent implements OnInit {

  country!: Country;

  constructor(private activatedRoute: ActivatedRoute,
              private countryService: CountryService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        switchMap(
          ({id}) => this.countryService.getCountryAlpha(id)
        ),
        tap(console.log)
      )
      .subscribe(resp => this.country = resp);

    /*    this.activatedRoute.params
          .subscribe(({id}) => {
          this.countryService.getCountryAlpha(id)
            .subscribe((country: Country) => console.log(country));
          });*/
  }

}
