import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-city-input',
  templateUrl: './city-input.component.html',
  styleUrls: ['./city-input.component.scss']
})
export class CityInputComponent implements OnInit {

  @Output() onEvent: EventEmitter<string> = new EventEmitter<string>();
  @Output() onDebounce: EventEmitter<string> = new EventEmitter<string>();
  @Input() placeholderTitle: string = 'Search ...';

  debouncer: Subject<string> = new Subject<string>();

  term: string = '';

  constructor() {
  }

  ngOnInit(): void {
    this.debouncer.
      pipe(debounceTime(500))
      .subscribe((value: string) => this.onDebounce.emit(value));
  }

  searchCountry() {
    this.onEvent.emit(this.term);
  }

  keyboardEvent(){
    this.debouncer.next(this.term);
  }
}
