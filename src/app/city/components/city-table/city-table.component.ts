import {Component, Input, OnInit} from '@angular/core';
import {Country} from '../../interfaces/country.interface';

@Component({
  selector: 'app-city-table',
  templateUrl: './city-table.component.html',
  styleUrls: ['./city-table.component.scss']
})
export class CityTableComponent implements OnInit {

  @Input() resultCountry: Country[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
