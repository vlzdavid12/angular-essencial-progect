import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ForCountryComponent} from './city/pages/for-country/for-country.component';
import {ForCityComponent} from './city/pages/for-city/for-city.component';
import {ForCapitalComponent} from './city/pages/for-capital/for-capital.component';
import {ShowCountryComponent} from './city/pages/show-country/show-country.component';

const routes: Routes = [
  {
    path: '',
    component: ForCountryComponent,
    pathMatch: 'full'
  },
  {
    path: 'city',
    component: ForCityComponent
  },
  {
    path: 'capital',
    component: ForCapitalComponent
  },
  {
    path: 'country/:id',
    component: ShowCountryComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
